package projects.sorting;

public class Main {
    public static void main(String[] args) {
        SortHelper sh = new SortHelper();
        int[] arr = SortHelper.generateArray(10);
        Sorter sorter = new BubbleSort();
        printArray(arr);
        int[] sorted = sorter.sort(arr);
        printArray(sorted);

        int[] arr2 = SortHelper.generateArray(10);
        Sorter insertion = new InsertionSort();
        printArray(arr2);
        int[] sorted2 =insertion.sort(arr2);
        printArray(sorted2);

        int[] arr3 = SortHelper.generateArray(10);
        Sorter selection = new SelectionSort();
        printArray(arr3);
        int[] sorted3 = selection.sort(arr3);
        printArray(sorted3);

        int[] arr4 = SortHelper.generateArray(10);
        Sorter bucket = new BucketSort();
        printArray(arr4);
        int[] sorted4 = bucket.sort(arr4);
        printArray(sorted4);
    }

    private static void printArray(int[] arr) {
        for (int value: arr) {
            System.out.print(value + " - ");
        }
        System.out.println();
    }
}
