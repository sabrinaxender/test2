package projects.sorting;
import java.util.*;
import java.util.Collections;


public class BucketSort implements Sorter {
    @Override
    public int[] sort(int[] arr4) {
        int i, j;
        int maxlength = 0;
        int number;
        int temp;
        LinkedList<Integer> stack = new LinkedList<>();
        HashMap<Integer, ArrayList<Integer>> Sort = new HashMap<>();
        for (i = 0; i < arr4.length; i++) {
            if (lengthcounter(arr4[i]) > maxlength) {
                maxlength = arr4[i];
            }
        }
        for (i = 0; i < maxlength; i++) {
           Sort.clear();

            for (j = 0; j < arr4.length; j++) {
                number = arr4[j];

                stack.clear();
                while (number > 0) {
                    stack.add(number % 10);
                    number = number / 10;
                }
                if (stack.size() <= i) {
                    Sort.putIfAbsent(0, new ArrayList<Integer>());
                    Sort.get(0).add(arr4[j]);
                } else {
                    Sort.putIfAbsent(stack.get(i), new ArrayList<Integer>());
                    Sort.get(stack.get(i)).add(arr4[j]);
                }
            }
            temp=0;
            for(j=0; j<10; j++) {
                if (Sort.containsKey(j) == true) {
                    for (int k = 0; k < Sort.get(j).size(); k++) {
                        arr4[temp] = Sort.get(j).get(k);
                        temp = temp + 1;
                    }
                }
            }

        }
            return arr4;


    }
    private int lengthcounter(int number){
        int length = String.valueOf(number).length();
        return length;
    }
}

