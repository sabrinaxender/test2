package projects.sorting;

public class InsertionSort implements Sorter{
    @Override
    public int[] sort(int[] arr2) {
        for (int i = 1; i<arr2.length; i++) {
            int k = arr2[i];
            int j = i-1;
            while ( (j > -1) && (arr2[j] > k)) {
                arr2[j+1] = arr2[j];
                j--;
            }
            arr2[j+1] = k;
        }
        return arr2;
    }
}
