package projects.sorting;

public class SelectionSort implements Sorter {

    @Override
    public int[] sort(int[] arr3) {
        for (int i =0; i < arr3.length - 1; i++) {
            int k = i;
            for (int j = i +1; j < arr3.length; j++) {
                if (arr3[j] < arr3[k]) {
                    k = j;
                }
            }
            int sn = arr3[k]; //smaller number
            arr3[k] = arr3[i];
            arr3[i] = sn;
        }
        return arr3;
    }
}
