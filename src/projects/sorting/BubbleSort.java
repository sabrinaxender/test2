package projects.sorting;

public class BubbleSort implements Sorter{

    @Override
    public int[] sort(int[] arr) {
        for (int j = 0; j < arr.length; j++) {
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;

                }
            }
        }

        return arr;
    }
}
