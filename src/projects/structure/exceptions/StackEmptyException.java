package projects.structure.exceptions;

public class StackEmptyException extends Exception{
    public StackEmptyException(String message) {
        super(message);
    }
}

