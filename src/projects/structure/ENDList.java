package projects.structure;

public class ENDList {
    private Node root = null;

    public void addValue(int number) {
        Node node = new Node(number);
        if (root == null) {
            root = node;
        } else {
            Node n = getLastNode();
            n.setNext(node);
        }

    }

    public void printList(){
        Node n = this.root;
        while(n.getNext()!=null) {
            System.out.println("value: " + n.getNumber());
            n = n.getNext();
        }
        System.out.println("value: " + n.getNumber());
    }

    private Node getLastNode(){
        if (root==null) {
            return null;
            // Gefährlich
        }
        Node n = root;
        while (n.getNext()!=null){
            n = n.getNext();
        }
        return n;
    }

    public int get(int index) {
        // exception
        Node n = this.root;
        int cnt = 0;
        boolean found = false;
        while(!found) {
            if (index==cnt) {
                break;
            }
            n = n.getNext();
            cnt++;

        }
        return n.getNumber();
    }

    public void remove(int index){
        Node n = root;
        Node next;
        if (index == 0) {
            root = n.getNext();
        }
        else if(index == 1) {
            n = n.getNext();
            root.setNext(n.getNext());
            n = null;
        } else {
            for (int i = 0; i <= index-1; i++) {
                if(n.getNext() == null) {return;}
                n = n.getNext();
            }
            next = n.getNext();
            n.setNext(next.getNext());
            next = null;
        }
    }
    public void clear(){
        root = null;
    }
}
