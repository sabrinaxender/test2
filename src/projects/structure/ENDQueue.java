package projects.structure;

public class ENDQueue {
    private Node last;

    public void enqueue (int value) {
        Node n = new Node(value);
        if (last == null) {
            last = n;
        } else if(last.getNext() == null) {
            last.setNext(n);
        } else {
            Node x = last;
            while (x.getNext()!= null) {
                x = x.getNext();
            }
            x.setNext(n);
        }
    }

    public int dequeue() {
        int x = last.getNumber();
        last = last.getNext();
        return x;
    }
}
