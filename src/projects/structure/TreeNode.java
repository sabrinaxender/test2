package projects.structure;

import jdk.nashorn.api.tree.Tree;

public class TreeNode {
    private TreeNode child1;
    private TreeNode child2;
    private int value;

    public TreeNode(int value) {
        this.value = value;
    }

    public TreeNode getChild1() {
        return child1;
    }

    public void setChild1(TreeNode child1) {
        this.child1 = child1;
    }

    public TreeNode getChild2() {
        return child2;
    }

    public void setChild2(TreeNode child2) {
        this.child2 = child2;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
