package projects.structure;

public class ENDTree {
    TreeNode root;
    public void add (int value) {
        boolean loop = true;
        TreeNode n = new TreeNode(value);
        if (root == null) {
            root = n;
        } else {
            TreeNode x = root;
            while (loop) {
                if (x.getValue() <= n.getValue()) {
                    if (x.getChild1() == null) {
                        x.setChild1(n);
                        return;
                    } else {
                        x = x.getChild1();
                    }

                } else {
                    if (x.getChild2() == null) {
                        x.setChild2(n);
                        return;
                    } else {
                        x = x.getChild2();
                    }
                }
            }
        }
    }
    public void clear() {
        root = null;
    }
}
